package logica;

import java.util.LinkedList;
import java.util.Random;
import java.util.Set;

public class MotorDeJuego {
	private Grafo grafo;
	private boolean [][] matrizBooleana;
	private int largo;
	private int contadorCambiarPunto;
	
	public MotorDeJuego(int largo){
		verificarIntMayorCero(largo);
		this.largo = largo;
		this.grafo = initGrafo(largo);
		this.matrizBooleana = initMat(grafo, largo);
		contadorCambiarPunto = 0;
	}
	
	// iniciar el grafo con sus aristas
	private static Grafo initGrafo (int largo){
		Grafo ret = new Grafo(largo * largo);
		
		int validacionResto = (largo-1 % largo);
		
		for(int i = 0;i < (largo*largo); i++){
			if(ret.existePunto(i + 1) && (i+1)%largo != 0){
				if(!ret.existeArista(i, i + 1)){
					ret.agregarArista(i, i + 1);
				}
			}
			if(ret.existePunto(i - 1) && (i-1)%largo != validacionResto){
				if(!ret.existeArista(i, i - 1)){
					ret.agregarArista(i, i - 1);
				}
			}
			if(ret.existePunto(i + largo)){
				if(!ret.existeArista(i, i + largo)){
					ret.agregarArista(i, i + largo);
				}
			}
			
			if(ret.existePunto( i - largo)){
				if(!ret.existeArista(i, i - largo)){
					ret.agregarArista(i, i - largo);
				}
			}
		}
		return ret;
	}
	
	//Iniciar una matriz booleana con valores random
	private boolean[][] initMat(Grafo grafo2, int largo2) {
		boolean [][] ret = new boolean [this.largo][this.largo];
		for (int i = 0; i< ret.length; i++) {
			Random rd = new Random();
			for (int j = 0; j<ret[i].length; j++) {
				ret[i][j] = rd.nextBoolean();
			}
		}
		return ret;
	}
	
	// seleccionar una punto y sus aristas
	public boolean[][] cambiarPunto(int punto) {
		verificarIntEnMat(punto);
		
		Set<Integer> puntosParaCambiarSet = grafo.vecinos(punto);
		puntosParaCambiarSet.add(punto);
		changeMat(puntosParaCambiarSet);
		
		contadorCambiarPunto ++;
		
		return this.matrizBooleana;	
	}
	
	// modificar la matris con un grupo de puntos, todos estos puntos cambian su valor a su inversa
	private void changeMat(Set<Integer> puntosParaCambiarSet) {
		for(Integer posicion : puntosParaCambiarSet) {
			if (posicion >= this.largo) {
				this.matrizBooleana[posicion/this.largo][posicion%this.largo] = !this.matrizBooleana[posicion/this.largo][posicion%this.largo];
			}
			if(posicion < this.largo) {
				this.matrizBooleana[0][posicion] = !this.matrizBooleana[0][posicion];
			}
		}
	}

	private void verificarIntEnMat(int punto) {
		if(punto < 0) {
			throw new IllegalArgumentException("El valor: " + punto + " tiene que ser mayor o igual a 0 la matriz");
		}
		if(punto > this.largo*this.largo) {
			throw new IllegalArgumentException("El valor: " + punto + " tiene que ser menor al largo de la matriz");
		}
		
	}
	
	private void verificarIntMayorCero(int x) {
		if(x <= 0) {
			throw new IllegalArgumentException("El valor: " + x + " tiene que ser mayor a cero");
		}
		
	}
	
	public int getContador() {
		return contadorCambiarPunto;
	}
	
	public LinkedList<Boolean>  getBoolList () {
		boolean[][] ret = this.matrizBooleana;
		LinkedList<Boolean> nahuetest = new LinkedList<Boolean>();
		for (int i = 0; i< ret.length; i++) {
			for (int j = 0; j<ret[i].length; j++) {
				boolean bool = ret[i][j];
				nahuetest.add(new Boolean(bool));
			}
		}
		return nahuetest;
	}
	
	public boolean ganarJuego() {
		boolean seGano = true;
		for (int i = 0; i< matrizBooleana.length; i++) {
			for (int j = 0; j<matrizBooleana[i].length; j++) {
				seGano = seGano && !matrizBooleana[i][j];
			}
		}
		return seGano;
	}
	
	public int calcularPunutacion(int tiempoEnSegundos) {
		return (((10000*this.largo)/contadorCambiarPunto /tiempoEnSegundos)*1000);
	}
}