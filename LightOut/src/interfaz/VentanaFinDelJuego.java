package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Rectangle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logica.Puntuacion;
import logica.PuntuacionItem;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;

public class VentanaFinDelJuego extends JFrame {

	private JPanel contentPane;
	private Puntuacion p;
	private JTextField textField;
	private LinkedList<PuntuacionItem> piList;

	public VentanaFinDelJuego(int posx, int posy, int puntuacion) {
		p = new Puntuacion();
		piList = p.getPuntuacionList();
		iniciarVentana(posx, posy);
		boolean esPuntuacionAlta= esPuntuacionAlta(puntuacion);
		iniciarContenido(puntuacion, esPuntuacionAlta);
	}
	
	public VentanaFinDelJuego(int posx, int posy, int puntuacion, boolean esPuntuacionAlta) {
		p = new Puntuacion();
		piList = p.getPuntuacionList();
		iniciarVentana(posx, posy);
		iniciarContenido(puntuacion, esPuntuacionAlta);
	}
	
	private boolean esPuntuacionAlta(int puntuacion) {
		boolean ret = false;
		if(piList.isEmpty() || piList.size() < 3)
			ret = true;
		else {
			for(PuntuacionItem pi : piList) {
				if(pi.getPuntuacion() <= puntuacion) {
					ret = true;
				}
			}
		}
		return ret;
	}
	
	private void iniciarContenido(int puntuacion, boolean esPuntuacionAlta) {
		JLabel lblNewLabel = new JLabel("\u00A1\u00A1 Ganaste !!");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 48));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(60, 23, 284, 45);
		contentPane.add(lblNewLabel);
		
		agregarPuntuacion(esPuntuacionAlta);
		
		JButton btnVolverAJugar = new JButton("Volver a jugar");
		btnVolverAJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iniciarVentanaInicio();
			}
		});
		btnVolverAJugar.setBounds(150, 227, 120, 23);
		btnVolverAJugar.setVisible(!esPuntuacionAlta);
		contentPane.add(btnVolverAJugar);
		
		//Elejir un nombre		
		JLabel lblNewLabel_1 = new JLabel("Elije un nombre:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(67, 103, 261, 19);
		lblNewLabel_1.setVisible(esPuntuacionAlta);
		contentPane.add(lblNewLabel_1);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				crearPuntuacion(puntuacion, btnAceptar, lblNewLabel_1);
			}
		});
		
		textField = new JTextField();
		textField.setBounds(67, 133, 162, 20);
		textField.setVisible(esPuntuacionAlta);
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(arg0.getKeyCode() == 10) {
					crearPuntuacion(puntuacion, btnAceptar, lblNewLabel_1);
				}
			}
		});
		contentPane.add(textField);
		textField.setColumns(10);
		
		btnAceptar.setBounds(239, 132, 89, 23);
		btnAceptar.setVisible(esPuntuacionAlta);
		contentPane.add(btnAceptar);
		
	}
	
	private void agregarPuntuacion(boolean esPuntuacionAlta) {
		if(!esPuntuacionAlta) {
			agregarPuntuacion();
		}
	}
	
	private void agregarPuntuacion() {
		if(!piList.isEmpty()) {
			JLabel label = new JLabel("1."+piList.get(0).getNombre()+" Punutacion: "+piList.get(0).getPuntuacion());
			label.setFont(new Font("Tahoma", Font.PLAIN, 16));
			label.setBounds(43, 86, 261, 19);
			label.setVisible(true);
			contentPane.add(label);
			if(piList.size() >= 2) {
				JLabel label_1 = new JLabel("2."+piList.get(1).getNombre()+" Punutacion: "+piList.get(1).getPuntuacion());
				label_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
				label_1.setBounds(43, 117, 261, 19);
				label_1.setVisible(true);
				contentPane.add(label_1);
			}
			if(piList.size() >= 3) {
				JLabel label_2 = new JLabel("3."+piList.get(2).getNombre()+" Punutacion: "+piList.get(2).getPuntuacion());
				label_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
				label_2.setBounds(43, 151, 261, 19);
				label_2.setVisible(true);
				contentPane.add(label_2);
			}
		}
	}
	
	private void crearPuntuacion(int puntuacion, JButton btnAceptar, JLabel lblNewLabel_1) {
		p.agregarPuntuacion(textField.getText(), puntuacion);
		this.piList = p.getPuntuacionList();
		Rectangle rectVentana = getBounds();
		VentanaFinDelJuego vfdj = new VentanaFinDelJuego(rectVentana.x, rectVentana.y, puntuacion, false);
		vfdj.setVisible(true);
		dispose();
	}
	
	private void iniciarVentana(int posx, int posy) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(posx, posy, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}
	
	private void iniciarVentanaInicio() {
		Rectangle rectVentana = getBounds();
		VentanaInicio vfdj = new VentanaInicio(rectVentana.x, rectVentana.y);
		vfdj.setVisible(true);
		dispose();
	}
}