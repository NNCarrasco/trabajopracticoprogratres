package logica;

public class PuntuacionItem {
	private String nombre;
	private int puntos;
	
	public PuntuacionItem( String nombre, int puntos) {
		if(nombre == null) {
			throw new NullPointerException("El nombre no puede ser nulo");
		}
		this.nombre = nombre;
		this.puntos = puntos;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public int getPuntuacion() {
		return puntos;
	}
}
