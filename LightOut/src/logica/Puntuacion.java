package logica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.LinkedList;

public class Puntuacion {
	LinkedList<PuntuacionItem> pItemList;
	File archivo = null;
	
	public Puntuacion() {
		FileReader fr = null;
		
		pItemList = new LinkedList<PuntuacionItem>();
				 
		try {
			// Apertura del fichero y creacion de BufferedReader 
			archivo = new File ("Puntuacion.txt");
			fr = new FileReader (archivo);
			BufferedReader br = new BufferedReader(fr);

         	// Lectura del fichero
         	String linea;
         	if(br.ready()) {
         		while((linea=br.readLine())!=null) {
         			pItemList.add(new PuntuacionItem(linea.split(";")[0], Integer.parseInt(linea.split(";")[1])));
         		}
         	}
		} catch(Exception e){
			e.printStackTrace();
		} finally{
			// se cierra el fichero
			try{                    
				if( null != fr ){   
					fr.close();     
				}                  
			}catch (Exception e2){ 
				e2.printStackTrace();
			}
		}
	}
	
	public void agregarPuntuacion(String nombre, int puntos) {
		LinkedList<PuntuacionItem> aux = new LinkedList<PuntuacionItem>();
		boolean seAgrego = false;
		for (PuntuacionItem pi : pItemList) {
			if(pi.getPuntuacion() <= puntos && !seAgrego) {
				seAgrego = true;
				aux.add(new PuntuacionItem(nombre, puntos));
			}
			aux.add(pi);
		}
		if(!seAgrego)
			aux.add(new PuntuacionItem(nombre, puntos));
		pItemList = aux;
		actualizarArchivo();
	}
	
	private void actualizarArchivo() {
		FileWriter fichero = null;
        PrintWriter pw = null;
        try{
        	// Apertura del fichero y creacion de FileWriter
            fichero = new FileWriter("Puntuacion.txt");
            pw = new PrintWriter(fichero);

            for (PuntuacionItem pi : pItemList) 
                pw.println(pi.getNombre()+";"+pi.getPuntuacion());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // se cierra el fichero
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
	}
	
	public LinkedList<PuntuacionItem> getPuntuacionList(){
		return pItemList;
	}
}