package logica;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PuntuacionItemTest {

	@Test
	void getNombre() {
		PuntuacionItem n = new PuntuacionItem("Test",1);
		assertEquals("Test", n.getNombre());
	}
	
	@Test
	void getPuntuacion() {
		PuntuacionItem n = new PuntuacionItem("Test",1);
		assertEquals(1, n.getPuntuacion());
	}
	
	@Test
	void setNombreNulo() {
	  Assertions.assertThrows(NullPointerException.class, () -> {
		  PuntuacionItem n = new PuntuacionItem(null,1);
	  });
	}
}
