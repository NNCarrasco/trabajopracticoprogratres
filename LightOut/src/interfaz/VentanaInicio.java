package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Rectangle;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;
import javax.swing.DropMode;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class VentanaInicio extends JFrame {

	private JPanel contentPane;
	private JTextField txtQwe;

	public VentanaInicio() {
		iniciarVentana(100,100);
		asignarContenido();
	}
	
	public VentanaInicio(int posx, int posy) {
		iniciarVentana(posx,posy);
		asignarContenido();
	}
	
	private void iniciarVentana(int posx, int posy) {
		setTitle("Light Out");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(posx, posy, 300, 185);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}

	private void asignarContenido() {
		JLabel lbl_titulo = new JLabel("Seleccion de modo de juego");
		lbl_titulo.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_titulo.setBounds(20, 11, 243, 36);
		contentPane.add(lbl_titulo);
		
		JButton btn_tres = new JButton("3 x 3");
		btn_tres.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iniciarJuego(3);
			}
		});
		btn_tres.setBounds(20, 58, 243, 23);
		contentPane.add(btn_tres);
		
		JButton btn_cinco = new JButton("5 x 5");
		btn_cinco.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iniciarJuego(5);
			}
		});
		btn_cinco.setBounds(20, 89, 243, 23);
		contentPane.add(btn_cinco);
		
		JCheckBox cb_isPersonalizado = new JCheckBox("Personalizado?");
		cb_isPersonalizado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(cb_isPersonalizado.isSelected()) {
					setSize(300, 250);
				}else {
					setSize(300, 185);
				}
			}
		});
		
		cb_isPersonalizado.setBounds(20, 119, 243, 23);
		contentPane.add(cb_isPersonalizado);
		
		JLabel lbl_textoAyuda = new JLabel("<html><body>Elije un n� natural mayor o igual a 3<br> de max 1 digito.</body></html>");
		lbl_textoAyuda.setBounds(20, 153, 243, 33);
		contentPane.add(lbl_textoAyuda);
		
		JTextField tp_numeroPersonalizado = new JTextField();
		tp_numeroPersonalizado.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(arg0.getKeyCode() == 10) {
					if(validarTexto(tp_numeroPersonalizado.getText())) {
						iniciarJuego(Integer.parseInt(tp_numeroPersonalizado.getText()));
					}else {
						JOptionPane.showMessageDialog(null, "Favor de elijir un numero natural mayor o igual a 3 de max 1 digito");
						tp_numeroPersonalizado.setText("");
					}
				}
			}
		});
		tp_numeroPersonalizado.setBounds(20, 188, 35, 23);
		contentPane.add(tp_numeroPersonalizado);
		
		JButton btn_usarPersonalizado = new JButton("Ok");
		btn_usarPersonalizado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(validarTexto(tp_numeroPersonalizado.getText())) {
					iniciarJuego(Integer.parseInt(tp_numeroPersonalizado.getText()));
				}else {
					JOptionPane.showMessageDialog(null, "Favor de elijir un numero natural mayor o igual a 3 de max 1 digito");
					tp_numeroPersonalizado.removeAll();
				}
			}
		});	
		btn_usarPersonalizado.setBounds(64, 188, 91, 23);
		contentPane.add(btn_usarPersonalizado);				
	}
	
	private void iniciarJuego(int valor) {
		Rectangle rectVentana = getBounds();
		VentanaJuego vj = new VentanaJuego(rectVentana.x, rectVentana.y, valor);
		vj.setVisible(true);
		dispose();
	}

	private boolean validarTexto(String text) {
		Integer num;
		if(text == null)
			return false;
		try {
            num = Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return false;
        }
		if( num == 0 || num <3 || (int)(Math.log10(num) + 1) > 1) 
			return false;
		return true;
	}
}