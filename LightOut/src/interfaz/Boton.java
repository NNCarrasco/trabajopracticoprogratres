package interfaz;

import java.awt.Container;
import java.awt.Image;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Boton {

	private JButton boton;
	private int id;
	private ImageIcon icono;
	private ImageIcon imagen;
	private ImageIcon imagen2;
	
	
	public Boton(int posX, int posY, int ancho, int alto, int id){
		
		boton = new JButton();
		setBounds(posX, posY, ancho, alto);
		imagen = new ImageIcon ("lamparaOn.png");
		imagen2 = new ImageIcon ("lamparaOff.png");
		
		this.id=id;
		icono = new ImageIcon(imagen2.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
		//boton.setIcon(icono);
		boton.setIcon(imagen2);
		boton.setVisible(true);
	}
	
	public void actualizarImagen(Boolean valor){
		
		if (valor){
			
			icono = new ImageIcon(imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
			boton.setIcon(icono);
		}
			
		else{
			icono = new ImageIcon(imagen2.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
			boton.setIcon(icono);
		}
			
	}
	
	public void dibujarBoton(Container cp){
		cp.add(boton);
	}
	public int getId(){
		return id;
	}

	public void setBounds(int posX, int posY, int ancho, int alto) {
		boton.setBounds(posX, posY, ancho, alto);
	}

	public void setActionListener(ActionListener accion) {
		boton.addActionListener(accion);
		
	}

	public Object getJButton() {
		return boton;
	}
}
