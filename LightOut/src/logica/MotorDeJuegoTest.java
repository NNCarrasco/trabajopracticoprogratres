package logica;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MotorDeJuegoTest {

	@Test
	void contructorConLargoCeroTest() {
		IllegalArgumentException error = assertThrows(IllegalArgumentException.class,
	            ()->{
	            	MotorDeJuego sn = new MotorDeJuego(0);
	            });
		assertTrue(error.getMessage() != null);
	}
	
	@Test
	void constructorConLargoNegativoTest() {
		IllegalArgumentException error = assertThrows(IllegalArgumentException.class,
	            ()->{
	            	MotorDeJuego sn = new MotorDeJuego(-1);
	            });
		assertTrue(error.getMessage() != null);
	}
	
	@Test
	void cambiarPuntoConValorNegativoTest() {
		MotorDeJuego sn = new MotorDeJuego(3);
				
		IllegalArgumentException error = assertThrows(IllegalArgumentException.class,
	            ()->{
	        		boolean[][] matBool = sn.cambiarPunto(-1);
	            });
	            
		assertTrue(error.getMessage() != null);
	}
	
	@Test
	void cambiarPuntoConValorMayorAlLargoDelGrafoTest() {
		MotorDeJuego sn = new MotorDeJuego(3);
		
		IllegalArgumentException error = assertThrows(IllegalArgumentException.class,
	            ()->{
	        		boolean[][] matBool = sn.cambiarPunto(9);
	            });
	            
		assertTrue(error.getMessage() != null);
	}
	@Test
	void calcularPunutacion() {
		MotorDeJuego sn = new MotorDeJuego(3);
		
		sn.cambiarPunto(0);

		assertEquals(30000000, sn.calcularPunutacion(1));
	}
}
