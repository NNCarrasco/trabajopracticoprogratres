package logica;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

public class GrafoTest
{
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaConIndiceNegativoTest(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(-1, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaConIndiceExcedidoTest(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(3, 5);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaConIndicesIgualesTest(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(3, 3);
	}
	
	@Test
	public void agregarAristaCorrectaTest(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		assertTrue( grafo.existeArista(2, 3) );
	}

	@Test
	public void aristasSimetricasTest(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		assertTrue( grafo.existeArista(3, 2) );
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaDuplicadaTest(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(2, 3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void eliminarAristaConIndiceNegativoTest(){
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(-1, 3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void eliminarAristaInexistenteTest(){
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(1, 3);
	}
	
	@Test
	public void eliminarAristaCorrectaTest(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4);
		grafo.eliminarArista(2, 4);

		assertFalse( grafo.existeArista(2, 4) );
	}
	
	@Test
	public void eliminarAristaInvertidaTest(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 4);
		grafo.eliminarArista(2, 4);

		assertFalse( grafo.existeArista(4, 2) );
	}

	@Test(expected = IllegalArgumentException.class)
	public void existeAristaConIndiceNegativoTest(){
		Grafo grafo = new Grafo(5);
		grafo.existeArista(2, -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void existeAristaConIndiceExcedidoTest(){
		Grafo grafo = new Grafo(5);
		grafo.existeArista(2, 5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void vecinosDeVerticeNegativoTest(){
		Grafo grafo = new Grafo(5);
		grafo.vecinos(-1);
	}
	
	@Test
	public void vecinosDeUnVerticeUniversalTest(){
		Grafo grafo = casitaCruzada();
		int[] esperado = {1, 2, 3, 4};
		Assert.iguales(esperado, grafo.vecinos(0));
	}

	@Test
	public void vecinosDeUnVerticeNoUniversalTest(){
		Grafo grafo = casitaCruzada();
		int[] esperado = {0, 2};
		Assert.iguales(esperado, grafo.vecinos(1));
	}

	@Test
	public void vecinosDeUnVerticeAisladoTest(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		
		int[] esperado = { };
		Assert.iguales(esperado, grafo.vecinos(1));
	}
	
	private Grafo casitaCruzada(){
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(0, 4);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(3, 4);
		
		return grafo;
	}
}
