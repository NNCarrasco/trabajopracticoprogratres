package interfaz;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logica.MotorDeJuego;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JButton;

public class VentanaJuego extends JFrame {

	private JPanel contentPane;
	private int raizMatriz;
	private MotorDeJuego mj;
	private LinkedList<Boton> b2 = new LinkedList<Boton>();
	private JLabel lbl_contador;
	private int tiempoEnSegundos = 1;
	private Clip audio;
	
	public VentanaJuego(int posx, int posy, int value) {
		raizMatriz = value;
		mj = new MotorDeJuego(raizMatriz);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		int ancho = 5 + 80 * value;
		int alto = 30 + 28 + 80 * value;
		
		setBounds(posx, posy, ancho, alto);
		
		sonido("Shoot.wav");
		ventJuego(contentPane);
	}
	
	public void sonido(String sonido) {
		
		try{
			audio = AudioSystem.getClip();
			audio.open(AudioSystem.getAudioInputStream(new File(sonido)));
			audio.start();
		
		}
		catch(LineUnavailableException | UnsupportedAudioFileException | IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	private void ventJuego(Container contpane){
		
		lbl_contador = new JLabel("Contador: 0");
		lbl_contador.setBounds(0, 0, 100, 30);
		contpane.add(lbl_contador);
		ActionListener botonAccion= new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int id = 8;
				for(Boton btn : b2){
					if (arg0.getSource().equals( btn.getJButton())){
						id = btn.getId();
					}
				}
				actualizarBotones(id);
			}
		};
		int counter = 0;
		int ultimoY = 30;
		int largoX = 0;
		for(int i =0; i < raizMatriz; i++){
			int ultimoX = 0;
			for (int j =0; j < raizMatriz; j++){
				Boton btn = new Boton(ultimoX,ultimoY,80,80, counter);
				btn.setActionListener(botonAccion);
				btn.dibujarBoton(contpane);
				b2.add(btn);
				if(j == raizMatriz-1){
					largoX = ultimoX;
					ultimoY+=80;
				}
				else {
					ultimoX +=80;
				}
				counter ++;
			}
		}
		actualizarBotones();
	}

	protected void actualizarBotones(int id) {
		mj.cambiarPunto(id);
		actualizarContador(mj.getContador());
		actualizarBotones();
		
	}
	
	private void actualizarContador(int contador) {
		lbl_contador.setText("Contador: " + contador);		
	}
	
	private void actualizarBotones() {
		LinkedList<Boolean> boolList = mj.getBoolList();
		for (int i = 0; i< b2.size(); i++){
			b2.get(i).actualizarImagen(boolList.get(i));
		}
		seGano();
	}
	
	private void seGano() {
		if( mj.ganarJuego() )
			ganarJuego();
	}
	
	private void ganarJuego() {
		Rectangle rectVentana = getBounds();
		VentanaFinDelJuego vfdj = new VentanaFinDelJuego(rectVentana.x, rectVentana.y, mj.calcularPunutacion(tiempoEnSegundos));
		vfdj.setVisible(true);
		dispose();
	}
}
